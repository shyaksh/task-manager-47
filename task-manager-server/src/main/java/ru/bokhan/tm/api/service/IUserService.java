package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;

public interface IUserService extends IService<UserDTO, User> {

    void create(@Nullable String login, @Nullable String password);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void updateById(
            @Nullable String id, String login,
            @Nullable String firstName, @Nullable String lastName, @Nullable String middleName,
            @Nullable String email
    );

    void updatePasswordById(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
