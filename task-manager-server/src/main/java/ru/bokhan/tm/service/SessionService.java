package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IPropertyService;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.entity.Session;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.repository.dto.SessionRepositoryDTO;
import ru.bokhan.tm.repository.entity.SessionRepository;
import ru.bokhan.tm.util.HashUtil;
import ru.bokhan.tm.util.SignatureUtil;

import java.util.List;

@Service
public class SessionService extends AbstractService<SessionDTO, Session> implements ISessionService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionService service;

    @NotNull
    @Autowired
    private SessionRepositoryDTO dtoRepository;

    @NotNull
    @Autowired
    private SessionRepository repository;

    @Override
    @Transactional
    public void close(@NotNull final SessionDTO session) throws AccessDeniedException {
        service.validate(session);
        repository.deleteById(session.getId());
    }

    @Override
    @Transactional
    public void closeAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        repository.deleteAll();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public UserDTO getUser(@NotNull final SessionDTO session) throws AccessDeniedException {
        @NotNull final String userId = getUserId(session);
        return userService.findById(userId);
    }

    @NotNull
    @Override
    @Transactional(readOnly = true)
    public String getUserId(@NotNull final SessionDTO session) throws AccessDeniedException {
        service.validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return userId;
    }

    @NotNull
    @Override
    @Transactional(readOnly = true)
    public List<SessionDTO> findAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        service.validate(session, Role.ADMIN);
        return dtoRepository.findAll();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public SessionDTO sign(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final SessionDTO session = dtoRepository.findById(sessionDTO.getId()).orElse(null);
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@NotNull final SessionDTO session) {
        try {
            service.validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public void validate(@Nullable final SessionDTO sessionDTO) throws AccessDeniedException {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null) throw new AccessDeniedException();
        if (sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @Nullable final SessionDTO tempSigned = service.sign(temp);
        if (tempSigned == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = tempSigned.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!dtoRepository.existsById(sessionDTO.getId())) throw new AccessDeniedException();
    }

    @Override
    @Transactional(readOnly = true)
    public void validate(@NotNull final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        service.validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public SessionDTO open(@NotNull final String login, @NotNull final String password) {
        final boolean check = service.checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        dtoRepository.save(session);
        return service.sign(session);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional(readOnly = true)
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) return;
        @NotNull final String userId = user.getId();
        service.signOutByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.deleteByUserId(userId);
    }

}
