package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;
import ru.bokhan.tm.repository.dto.TaskRepositoryDTO;
import ru.bokhan.tm.repository.entity.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<TaskDTO, Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepositoryDTO dtoRepository;

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        save(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDTO == null) throw new EmptyUserIdException();
        taskDTO.setUserId(userId);
        save(taskDTO);
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.findByUserId(userId);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Nullable
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final List<TaskDTO> list = findAll(userId);
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    @Nullable
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) return;
        repository.deleteById(task.getId());
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) throw new IncorrectIdException();
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) throw new IncorrectIndexException();
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

}