# PROJECT INFO
TASK MANAGER

# DEVELOPER INFO

**NAME:** PAVEL BOKHAN

**E-MAIL:** SHYAKSH@GMAIL.COM

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROJECT BUILD

```bash
mvn clean install
```
# PROGRAM SERVER RUN

Using java:
```bash
java -jar ./task-manager-server/target/task-manager-server.jar
```
Using Docker:
```bash
docker-compose up -d
```

# PROGRAM SERVER SHUTDOWN

Using Docker:
```bash
docker-compose down
```

# PROGRAM CLIENT RUN

```bash
java -jar ./task-manager-client/target/task-manager-client.jar
```

# SCREENSHOTS
[Yandex.disk](https://yadi.sk/d/H17XZ7zY4bsPOA/JSE-48)
