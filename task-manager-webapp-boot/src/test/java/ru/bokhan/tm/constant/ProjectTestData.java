package ru.bokhan.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.dto.ProjectDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.bokhan.tm.constant.UserTestData.*;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDto USER1_PROJECT1 = new ProjectDto();

    @NotNull
    public final static ProjectDto USER1_PROJECT2 = new ProjectDto();

    @NotNull
    public final static ProjectDto USER1_NEW_PROJECT = new ProjectDto();

    @NotNull
    public final static ProjectDto ADMIN1_PROJECT1 = new ProjectDto();

    @NotNull
    public final static ProjectDto ADMIN1_PROJECT2 = new ProjectDto();

    @NotNull
    public final static ProjectDto NEW_USER_PROJECT1 = new ProjectDto();

    @NotNull
    public final static List<ProjectDto> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2);

    @NotNull
    public final static List<ProjectDto> ADMIN1_PROJECT_LIST = Arrays.asList(ADMIN1_PROJECT1, ADMIN1_PROJECT2);

    @NotNull
    public final static List<ProjectDto> NEW_USER_PROJECT_LIST = Collections.singletonList(NEW_USER_PROJECT1);

    @NotNull
    public final static List<ProjectDto> PROJECT_LIST = new ArrayList<>();

    static {
        PROJECT_LIST.addAll(USER1_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN1_PROJECT_LIST);
    }

    public static void initData() {
        USER1_PROJECT_LIST.forEach(project -> project.setUserId(USER1.getId()));
        ADMIN1_PROJECT_LIST.forEach(project -> project.setUserId(ADMIN1.getId()));

        for (int i = 0; i < PROJECT_LIST.size(); i++) {
            @NotNull final ProjectDto project = PROJECT_LIST.get(i);
            project.setName("project-" + i);
            project.setDescription("description of project-" + i);
        }

        NEW_USER_PROJECT1.setName("new-project");
        NEW_USER_PROJECT1.setUserId(NEW_USER.getId());

        USER1_NEW_PROJECT.setName("user-1-new-Project");
        USER1_NEW_PROJECT.setUserId(USER1.getId());
    }
}
