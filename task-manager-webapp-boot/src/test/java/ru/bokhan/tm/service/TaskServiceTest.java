package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.TaskManagerWebapp;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.dto.TaskDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.*;
import static ru.bokhan.tm.constant.RoleTestData.NEW_USER_ROLE;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.*;
import static ru.bokhan.tm.constant.UserTestData.*;


@Transactional
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerWebapp.class)
public class TaskServiceTest {

    @Autowired
    private ITaskService taskService;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        ProjectTestData.initData();
        TaskTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDto> list = taskService.findAll();
        Assert.assertEquals(TASK_LIST.size(), list.size());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, taskService.findAll());
    }

    @Test
    public void load() {
        // TODO: 03.03.2021
    }

    @Test
    public void delete() {
        Assert.assertTrue(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
        taskService.delete(USER1_TASK1);
        Assert.assertFalse(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void save() {
        Assert.assertFalse(taskService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_TASK1.getId()));
        entityManager.persist(NEW_USER);
        entityManager.persist(NEW_USER_ROLE);
        entityManager.persist(NEW_USER_PROJECT1);
        taskService.save(NEW_USER_TASK1);
        Assert.assertTrue(taskService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_TASK1.getId()));
    }

    @Test
    public void create() {
        @NotNull final TaskDto expected = new TaskDto();
        @NotNull final String taskName = "new";
        expected.setName(taskName);
        expected.setUserId(USER1.getId());
        taskService.create(USER1.getId(), USER1_PROJECT1.getId(), taskName);
        @Nullable final TaskDto actual = taskService.findByUserIdAndName(USER1.getId(), taskName);
        Assert.assertNotNull(actual);
        expected.setId(actual.getId());
        expected.setProjectId(actual.getProjectId());
        assertReflectionEquals(expected, actual);
    }

    @Test
    public void createWithDescription() {
        @NotNull final TaskDto expected = new TaskDto();
        @NotNull final String taskName = "new";
        @NotNull final String taskDescription = "new-task";
        expected.setName(taskName);
        expected.setDescription(taskDescription);
        expected.setUserId(USER1.getId());

        taskService.create(USER1.getId(), USER1_PROJECT1.getId(), taskName, taskDescription);
        @Nullable final TaskDto actual = taskService.findByUserIdAndName(USER1.getId(), taskName);
        Assert.assertNotNull(actual);
        expected.setId(actual.getId());
        expected.setProjectId(actual.getProjectId());
        assertReflectionEquals(expected, actual);
    }

    @Test
    public void saveByUserId() {
        Assert.assertFalse(taskService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_TASK1.getId()));
        entityManager.persist(NEW_USER);
        entityManager.persist(NEW_USER_ROLE);
        entityManager.persist(NEW_USER_PROJECT1);
        taskService.saveByUserId(NEW_USER.getId(), NEW_USER_TASK1);
        Assert.assertTrue(taskService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_TASK1.getId()));
    }

    @Test
    public void findAllByUserId() {
//        @NotNull final Set<Task> actual = new HashSet<>(taskService.findAllByUserId(USER1.getId()));
//        assertReflectionEquals(new HashSet<>(USER1_TASK_LIST), actual);
    }

    @Test
    public void deleteAllByUserId() {
        Assert.assertTrue(taskService.findAllByUserId(USER1.getId()).size() > 0);
        taskService.deleteAllByUserId(USER1.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, taskService.findAllByUserId(USER1.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        assertReflectionEquals(
                USER1_TASK1,
                taskService.findByUserIdAndId(USER1.getId(), USER1_TASK1.getId())
        );
    }

    @Test
    public void findByUserIdAndIndex() {
        assertThat(taskService.findByUserIdAndIndex(USER1.getId(), 1)).isEqualToIgnoringGivenFields(
                taskService.findAllByUserId(USER1.getId()).get(1),
                "project", "projectId", "user", "userId"
        );
//        assertReflectionEquals(
//                taskService.findAllByUserId(USER1.getId()).get(1),
//                taskService.findByUserIdAndIndex(USER1.getId(), 1)
//        );
    }

    @Test
    public void findByUserIdAndName() {
        assertReflectionEquals(
                USER1_TASK1,
                taskService.findByUserIdAndName(USER1.getId(), USER1_TASK1.getName())
        );
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertTrue(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
        taskService.deleteByUserIdAndId(USER1.getId(), USER1_TASK1.getId());
        Assert.assertFalse(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void deleteByUserIdAndIndex() {
        @Nullable final TaskDto second = taskService.findByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertNotNull(second);
        taskService.deleteByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertFalse(taskService.existsByUserIdAndId(USER1.getId(), second.getId()));
    }

    @Test
    public void deleteByUserIdAndName() {
        Assert.assertTrue(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
        taskService.deleteByUserIdAndName(USER1.getId(), USER1_TASK1.getName());
        Assert.assertFalse(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newName";
        taskService.updateById(
                USER1.getId(),
                USER1_TASK1.getId(),
                newName,
                newDescription
        );
        @Nullable final TaskDto actual =
                taskService.findByUserIdAndId(USER1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getName(), newName);
        Assert.assertEquals(actual.getDescription(), newDescription);
    }

    @Test
    public void updateByUserIdAndIndex() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newName";
        taskService.updateByUserIdAndIndex(
                USER1.getId(),
                1,
                newName,
                newDescription
        );
        @Nullable final TaskDto actual =
                taskService.findByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getName(), newName);
        Assert.assertEquals(actual.getDescription(), newDescription);
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertFalse(taskService.existsByUserIdAndId(ADMIN1.getId(), USER1_TASK1.getId()));
        Assert.assertTrue(taskService.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(taskService.countByUserId(USER1.getId()), USER1_TASK_LIST.size());
    }

}