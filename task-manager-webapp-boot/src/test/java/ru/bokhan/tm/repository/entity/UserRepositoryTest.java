package ru.bokhan.tm.repository.entity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.TaskManagerWebapp;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.bokhan.tm.constant.ProjectTestData.*;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.*;
import static ru.bokhan.tm.constant.UserTestData.USER1;
import static ru.bokhan.tm.constant.UserTestData.USER_LIST;

@Transactional
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerWebapp.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void findByLogin() {
        assertThat(userRepository.findByLogin(USER1.getLogin()))
                .isEqualToComparingOnlyGivenFields(
                        USER1,
                        "id", "login", "firstName", "lastName"
                );
    }

    @Test
    public void deleteByLogin() {
        Assert.assertTrue(userRepository.existsById(USER1.getId()));
        Assert.assertTrue(projectDtoRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertTrue(projectDtoRepository.existsById(USER1_PROJECT2.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK2.getId()));
        userRepository.deleteByLogin(USER1.getLogin());
        Assert.assertFalse(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(projectDtoRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectDtoRepository.existsById(USER1_PROJECT2.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK2.getId()));
    }

}