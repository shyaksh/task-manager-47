package ru.bokhan.tm.repository.dto;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.TaskManagerWebapp;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.PROJECT_LIST;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.*;
import static ru.bokhan.tm.constant.UserTestData.*;

@Transactional
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerWebapp.class)
public class TaskDtoRepositoryTest {

    @Autowired
    private TaskDtoRepository dtoRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void findAllByUserId() {
        assertReflectionEquals(
                USER1_TASK_LIST,
                dtoRepository.findAllByUserId(USER1.getId())
        );
    }

    @Test
    public void findByUserIdAndId() {
        assertReflectionEquals(
                USER1_TASK1,
                dtoRepository.findByUserIdAndId(USER1.getId(), USER1_TASK1.getId())
        );
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertFalse(dtoRepository.existsByUserIdAndId(ADMIN1.getId(), USER1_TASK1.getId()));
        Assert.assertTrue(dtoRepository.existsByUserIdAndId(USER1.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(dtoRepository.countByUserId(USER1.getId()), USER1_TASK_LIST.size());

    }

    @Test
    public void findByUserIdAndName() {
        assertReflectionEquals(
                USER1_TASK1,
                dtoRepository.findByUserIdAndName(USER1.getId(), USER1_TASK1.getName())
        );
    }

}