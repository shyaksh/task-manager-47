package ru.bokhan.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.dto.UserDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static ru.bokhan.tm.constant.ProjectTestData.PROJECT_LIST;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.TASK_LIST;
import static ru.bokhan.tm.constant.UserTestData.*;

@Transactional
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerWebapp.class)
public abstract class AbstractMockMvcTest {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected IUserService userService;

    @Autowired
    protected ITaskService taskService;

    @Autowired
    protected IProjectService projectService;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected AuthenticationManager authenticationManager;

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        initTestData();
        loginAsUser1();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    private void loginAsUser1() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(USER1.getLogin(), USER1.getLogin());
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private void loginAsAdmin1() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(ADMIN1.getLogin(), ADMIN1.getLogin());
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void initTestData() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        for (UserDto user : USER_LIST) {
            user.setPasswordHash(passwordEncoder.encode(user.getLogin()));
            entityManager.persist(user);
        }
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

}
