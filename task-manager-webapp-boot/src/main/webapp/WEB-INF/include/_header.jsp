<%--<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>--%>
<html>
<head>
    <title>TASK-MANAGER</title>
</head>
<style>
    h1 {
        font-size: 1.6em;

    }

    a {
        color: darkblue;
    }
</style>
<body>
<table border="1" width="1100px" align="center" style="border-collapse: collapse">
    <tr>
        <td width="200" height="35" nowrap="nowrap" align="center">
            <b><a href="/">TASK MANAGER</a></b>
        </td>
        <td width="100%" align="right">

            <a href="/projects">PROJECTS</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/tasks">TASKS</a>
<%--            <sec:authorize access="isAuthenticated()">--%>
                &nbsp;&nbsp;|&nbsp;&nbsp;
<%--                USER: <sec:authentication property="name"/>--%>
                &nbsp;&nbsp;
                <a href="/logout">LOGOUT</a>
                &nbsp;&nbsp;
<%--            </sec:authorize>--%>
<%--            <sec:authorize access="!isAuthenticated()">--%>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="/login">LOGIN</a>
                &nbsp;&nbsp
<%--            </sec:authorize>--%>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="padding: 10px;">
