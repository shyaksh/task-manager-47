package ru.bokhan.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface DataConstant {

    @NotNull
    String FILE_BINARY = "./data.bin";

    @NotNull
    String FILE_BASE64 = "./data.base64";

    @NotNull
    String FILE_XML = "./data.xml";

    @NotNull
    String FILE_JSON = "./data.json";

}
