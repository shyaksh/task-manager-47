package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.TaskDto;

import java.util.List;

public interface TaskDtoRepository extends AbstractDtoRepository<TaskDto> {

    @NotNull List<TaskDto> findAllByUserId(@NotNull final String userId);

    @NotNull TaskDto findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    long countByUserId(@NotNull final String userId);

    @Nullable TaskDto findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}