package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.bokhan.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @DateTimeFormat(pattern = "yyy-MM-dd")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "yyy-MM-dd")
    private Date dateFinish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @ManyToOne
    @JsonBackReference
    private Project project;

    @NotNull
    @ManyToOne
    @JsonBackReference
    private User user;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    public Task(
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) {
        super(id);
        this.name = name;
        this.description = description;
    }

}
