package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.repository.entity.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<TaskDto, Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskDtoRepository dtoRepository;

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        save(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    public void saveByUserId(@Nullable final String userId, @Nullable final TaskDto taskDto) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDto == null) throw new EmptyUserIdException();
        taskDto.setUserId(userId);
        save(taskDto);
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public TaskDto findByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public TaskDto findByUserIdAndIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final List<TaskDto> list = dtoRepository.findAllByUserId(userId);
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    @Nullable
    public TaskDto findByUserIdAndName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void deleteByUserIdAndIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final TaskDto task = findByUserIdAndIndex(userId, index);
        if (task == null) return;
        repository.deleteById(task.getId());
    }

    @Override
    @Transactional
    public void deleteByUserIdAndName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDto task = findByUserIdAndId(userId, id);
        if (task == null) throw new IncorrectIdException();
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    @Transactional
    public void updateByUserIdAndIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDto task = findByUserIdAndIndex(userId, index);
        if (task == null) throw new IncorrectIndexException();
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.countByUserId(userId);
    }

}