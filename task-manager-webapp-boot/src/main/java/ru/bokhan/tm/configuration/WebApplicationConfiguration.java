package ru.bokhan.tm.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.bokhan.tm.endpoint.soap.AuthenticationEndpoint;
import ru.bokhan.tm.endpoint.soap.ProjectEndpoint;
import ru.bokhan.tm.endpoint.soap.TaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/ws/*").permitAll()
                .antMatchers("/api/authentication/login/*").permitAll()
//                .antMatchers("/tasks").permitAll()
//                .antMatchers("/projects").permitAll()
//                .antMatchers("/free").permitAll()
//                .antMatchers("/admin").hasRole(RoleType.ADMIN.name())
//                .and()
//                .exceptionHandling()
//                .authenticationEntryPoint(new ServiceAuthenticationEntryPoint())
//                .and()
//                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/")
                .and()
                .csrf().disable();
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpoint projectEndpoint, @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpoint taskEndpoint, @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint authenticationEndpointRegistry(
            @NotNull final AuthenticationEndpoint authenticationEndpoint, @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authenticationEndpoint);
        endpoint.publish("/AuthenticationEndpoint");
        return endpoint;
    }

}
