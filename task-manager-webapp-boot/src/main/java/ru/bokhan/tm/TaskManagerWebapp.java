package ru.bokhan.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskManagerWebapp {

	public static void main(String[] args) {
		SpringApplication.run(TaskManagerWebapp.class, args);
	}

}
