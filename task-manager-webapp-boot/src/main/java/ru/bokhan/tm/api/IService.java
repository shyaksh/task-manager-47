package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.AbstractDto;
import ru.bokhan.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<D extends AbstractDto, E extends AbstractEntity> {

    @NotNull
    List<D> findAll();

    void clear();

    void load(@Nullable List<D> list);

    void delete(@Nullable E entity);

    @Nullable
    D save(@NotNull D entity);

    void delete(@NotNull D entity);

}
