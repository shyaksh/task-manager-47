package ru.bokhan.tm.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.bokhan.tm.AbstractMockMvcTest;
import ru.bokhan.tm.dto.ProjectDto;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_NEW_PROJECT;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.bokhan.tm.constant.UserTestData.USER1;

public class ProjectRestEndpointTest extends AbstractMockMvcTest {

    @NotNull
    private final String baseUrl = "/api/project";

    @Test
    public void save() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(USER1_NEW_PROJECT);
        mockMvc.perform(
                MockMvcRequestBuilders.post(baseUrl)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(USER1_NEW_PROJECT.getId()))
                .andExpect(jsonPath("$.userId").value(USER1.getId()));

        assertReflectionEquals(
                USER1_NEW_PROJECT,
                projectService.findByUserIdAndId(USER1.getId(), USER1_NEW_PROJECT.getId())
        );
    }

    @Test
    public void findById() throws Exception {
        @NotNull final String result = mockMvc.perform(
                MockMvcRequestBuilders.get(baseUrl + "/" + USER1_PROJECT1.getId())
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn()
                .getResponse()
                .getContentAsString();

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ProjectDto projectDto = objectMapper.readValue(result, ProjectDto.class);
        assertReflectionEquals(USER1_PROJECT1, projectDto);
    }

    @Test
    public void existsById() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get(baseUrl + "/exists/" + USER1_PROJECT1.getId())
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(baseUrl + "/" + USER1_PROJECT1.getId())
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

}