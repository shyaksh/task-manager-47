package ru.bokhan.tm.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.bokhan.tm.AbstractMockMvcTest;
import ru.bokhan.tm.dto.ProjectDto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_PROJECT_LIST;
import static ru.bokhan.tm.constant.UserTestData.USER1;

public class ProjectsRestEndpointTest extends AbstractMockMvcTest {

    @NotNull
    private final String baseUrl = "/api/projects";

    @Test
    public void findAll() throws Exception {
        @NotNull final String result = mockMvc.perform(
                MockMvcRequestBuilders.get(baseUrl)
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn()
                .getResponse()
                .getContentAsString();

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final List<ProjectDto> projects =
                Arrays.asList(objectMapper.readValue(result, ProjectDto[].class));
        assertReflectionEquals(USER1_PROJECT_LIST, projects);
    }

    @Test
    public void saveAll() throws Exception {
        projectService.deleteAllByUserId(USER1.getId());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(USER1_PROJECT_LIST);

        @NotNull final String result = mockMvc.perform(
                MockMvcRequestBuilders.post(baseUrl)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn()
                .getResponse()
                .getContentAsString();

        @NotNull final List<ProjectDto> projects =
                Arrays.asList(objectMapper.readValue(result, ProjectDto[].class));
        assertReflectionEquals(USER1_PROJECT_LIST, projects);
    }

    @Test
    public void count() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get(baseUrl + "/count")
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().string(String.valueOf(USER1_PROJECT_LIST.size())));
    }

    @Test
    public void deleteList() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(USER1_PROJECT_LIST);

        mockMvc.perform(
                MockMvcRequestBuilders.delete(baseUrl)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        Assert.assertEquals(Collections.emptyList(), projectService.findAllByUserId(USER1.getId()));
    }

    @Test
    public void deleteAll() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(baseUrl + "/all")
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        Assert.assertEquals(Collections.emptyList(), projectService.findAllByUserId(USER1.getId()));
    }

}