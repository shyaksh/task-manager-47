package ru.bokhan.tm.repository.dto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.configuration.WebApplicationConfiguration;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.PROJECT_LIST;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.TASK_LIST;
import static ru.bokhan.tm.constant.UserTestData.USER1;
import static ru.bokhan.tm.constant.UserTestData.USER_LIST;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserDtoRepositoryTest {

    @Autowired
    private UserDtoRepository dtoRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void findByLogin() {
        assertReflectionEquals(
                USER1,
                dtoRepository.findByLogin(USER1.getLogin())
        );
    }

}