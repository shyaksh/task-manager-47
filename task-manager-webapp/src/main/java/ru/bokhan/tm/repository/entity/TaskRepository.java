package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface TaskRepository extends AbstractRepository<Task> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}