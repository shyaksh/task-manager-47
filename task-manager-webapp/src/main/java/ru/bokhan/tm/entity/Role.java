package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @ManyToOne
    User user;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
