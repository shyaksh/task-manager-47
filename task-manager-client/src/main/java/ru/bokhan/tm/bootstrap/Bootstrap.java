package ru.bokhan.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;
import ru.bokhan.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    public void run(@Nullable final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        @NotNull String command = "";
        if (parseArguments(arguments)) System.exit(0);
        while (!"exit".equals(command)) {
            try {
                command = TerminalUtil.nextLine();
                if (command.isEmpty()) return;
                publisher.publishEvent(new ConsoleEvent(command));
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull final Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void parseArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return;
        @Nullable final AbstractListener listener = getListenerByArgument(argument);
        if (listener == null) return;
        publisher.publishEvent(new ConsoleEvent(listener.command()));
    }

    public boolean parseArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length == 0) return false;
        @Nullable final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @Nullable
    private AbstractListener getListenerByArgument(@Nullable final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.argument())) return listener;
        }
        return null;
    }

}