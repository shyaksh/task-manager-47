package ru.bokhan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.TaskEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class TaskByIdRemoveListener extends AbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    @EventListener(condition = "@taskByIdRemoveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(taskEndpoint);
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.deleteTaskById(id);
        System.out.println("[OK]");
    }

}
